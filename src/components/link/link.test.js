import React from 'react';
import Link from './index.js';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('Link Component', () => {
  test('[SnapShot] Link changes the class when hovered', () => {
    const component = renderer.create(
      <Link page="http://www.facebook.com" length="10">http://www.facebook.com</Link>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    // manually trigger the callback
    tree.props.onMouseEnter();
    // re-rendering
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    // manually trigger the callback
    tree.props.onMouseLeave();
    // re-rendering
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Make the url shorter', () => {
    const component = shallow(
      <Link page="http://www.facebook.com" length="10">http://www.facebook.com</Link>
    );
    expect(component.instance().shorten('http://www.facebook.com')).toEqual('http://www...');
  })
})