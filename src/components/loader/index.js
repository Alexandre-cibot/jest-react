import React from 'react';
import './loader.css';
import spinner from './spinning-circles.svg';

const Loader = ({ title }) => (
  <div className="loader">
    <img src={spinner} alt="spinner" />
    <br />
    <strong>{title}</strong>
  </div>
)

export default Loader;