import React from 'react';
import Articles from './index.js';
import Loader from '../loader';
import Article from '../article/index.js';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import API from '../../api/api-utils';
import { getArticles } from './__mocks__/index.js'

Enzyme.configure({ adapter: new Adapter() });

describe('Articles Component', () => {
  const component = shallow(
    <Articles />
  )
  it('Render loader when fetching articles', () => {
    expect(component.find(Loader).length).toBe(1);
  })

  it('Render articles when loaded', () => {
    jest.spyOn(API, 'getArticles').mockImplementation(getArticles)
    expect.assertions(1);
    return component.instance().fetchArticles().then(() => {
      expect(component.find(Article).length).toBe(2)
    })

    // component.setState({ articles: mockArticles }, () => {
    //   expect(component.find(Article).length).toBe(2)
    // })
  })
})