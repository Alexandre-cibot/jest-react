import React from 'react';
import Article from '../article';
import Loader from '../loader';
import API from '../../api/api-utils.js';
import './articles.css';

class Articles extends React.Component {
  constructor() {
    super();
    this.state = {
      articles: [],
      isFetching: false
    }
  }

  componentDidMount() {
    this.fetchArticles();
  }

  fetchArticles = async () => {
    this.setState({ isFetching: true });
    const articles = await API.getArticles();
    this.setState({
      articles,
      isFetching: false
    })
    return;
  }

  render() {
    const { isFetching } = this.state;
    const isEmpty = !this.state.articles.length && !isFetching;
    if (isEmpty) {
      return <strong>There is no Articles yet.</strong>
    }
    return (
      isFetching ?
        <Loader className="loader" title="Fetching articles ..." />
        :
        <>
          <div className="wrapper-btn-reload">
            <button onClick={this.fetchArticles}>Reload</button>
          </div>
          <div className="wrapper-articles">
            {this.state.articles.map((article, idx) => (
              <Article
                key={idx}
                title={article.title}
                content={article.content}
                description={article.description}
                url={article.url}
                imgUrl={article.urlToImage}
                source={article.source.name}
                index={idx} />
            ))}
          </div>
        </>
    )
  }
}

export default Articles