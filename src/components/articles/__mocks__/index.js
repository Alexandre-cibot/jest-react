export const getArticles = () => {
  return new Promise((res, rej) => {
    res(
      [{
        "source": {
          "id": "financial-post",
          "name": "Financial Post"
        },
        "author": "Bloomberg News",
        "title": "Scores of sleuths are searching for Quadriga’s frozen crypto coins — if they even exist at all",
        "description": "Some experts say the claim that the codes needed to unlock the crypto accounts are lost doesn't pass the smell test",
        "url": "https://business.financialpost.com/technology/blockchain/sleuths-scour-for-frozen-quadriga-coins-in-crypto-drama",
        "urlToImage": "https://financialpostcom.files.wordpress.com/2019/02/bitcoin-2.jpg",
        "publishedAt": "2019-02-07T16:30:02Z",
        "content": "The death of a crypto executive trapped $190 million on a Canadian exchange. Or did it?\r\nEver since Quadriga CX revealed last month that founder Gerald Cotten, who died in India in December, was the only person able to access the exchange’s digital ledgers, s… [+4379 chars]"
      }, {
        "source": {
          "id": null,
          "name": "Hackernoon.com"
        },
        "author": "Ermos Kyriakides",
        "title": "In the Looming Financial Crisis: What Will Happen To Bitcoin?",
        "description": "How will Bitcoin react once the impending financial crisis hits? Could it dissapear forever? Or shoot for the moon?",
        "url": "https://hackernoon.com/in-the-looming-financial-crisis-what-will-happen-to-bitcoin-71a6e079ccf0",
        "urlToImage": "https://cdn-images-1.medium.com/max/1200/0*p2OOBuEWn-3Wxbq0.jpg",
        "publishedAt": "2019-02-07T16:06:00Z",
        "content": "During a euphoric market, retail and institutions are more risk-seeking, investing in assets that have a higher yield potential. As shown in Figure A, high(er) risk assets, such as the VanGuard FTSE Emerging Markets ETF and the Australian Dollar, saw great re… [+6437 chars]"
      }]
    );
  })
}