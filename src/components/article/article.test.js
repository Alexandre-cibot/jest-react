import React from 'react';
import Artcile from './index';
import renderer from 'react-test-renderer';

test('[Snapshot] Article Component', () => {
  const component = renderer.create(
    <Artcile title="titre" content="contenu" url="www.monurl.com" index="0" />
  )
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})