import React from 'react';
import Link from '../link';
import './article.css';

const Article = ({ title, description, content, url, index, imgUrl }) => {
  const animationDelay = `${(index * 10) / 100}s`;
  return (
    <article className="article animated fadeInLeft" style={{ animationDelay }}>
      {/*  */}
      <div style={{ display: 'flex', justifyContent: 'center', flexGrow: 1, flexBasis: 0 }}>
        <img src={imgUrl} style={{ width: '100%', height: 'auto', objectFit: 'contain' }} alt='image url' />
      </div>
      <div style={{ flexGrow: 3, flexBasis: 0, paddingLeft: '20px', border: "1px solid" }}>
        <h2 className="title">{title}</h2>
        <div className="content">
          <p>{description}</p>
          <Link page="www.google.com" length="15">
            {url}
          </Link>
        </div>
      </div>
    </article>
  )
}
export default Article;