//https://newsapi.org/v2/everything?q=bitcoin&from=2019-01-07&sortBy=publishedAt&apiKey=9705ce250d0c4cc293f2b5042171708c

import axios from 'axios';
import { API_URL, API_KEY } from './globals.js';

// const getArticles = () => {
//   const articles = [
//     { title: 'Titre 1', content: 'Paragraphe 1', url: "www.lienversarticle1.com" },
//     { title: 'Titre 2', content: 'Paragraphe 2', url: "www.lienversarticle2.com" },
//     { title: 'Titre 3', content: 'Paragraphe 3', url: "www.lienversarticle3.com" },
//     { title: 'Titre 4', content: 'Paragraphe 4', url: "www.lienversarticle4.com" },
//   ]
//   return new Promise((res, rej) => {
//     setInterval(() => {
//       res(articles);
//     }, 2000);
//   })
// };

const getArticles = async () => {
  return (await axios.get(API_URL, {
    params: {
      q: 'bitcoin',
      from: '2019-06-26',
      sortBy: 'publishedAt',
      apiKey: API_KEY
    }
  })).data.articles;
}

export default {
  getArticles
}