import React, { Component } from 'react';
import './App.css';
import 'animate.css';
// Compponents
import Articles from './components/articles';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="header" style={{ textAlign: 'center' }}>
          <h1 className="App-logo-spin">The App.</h1>
        </header>
        <section>
          <Articles />
        </section>
      </div >
    );
  }
}

export default App;
